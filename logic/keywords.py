from functools import reduce
import aiohttp
import asyncio
import async_timeout
import string
from bs4 import BeautifulSoup


class KeywordCounter:

    def __init__(self, text_input, keywords_list):
        self.text = text_input
        self.keywords_string = keywords_list

    def _split_keywords_to_dict(self):
        _keywords = self.keywords_string.lower().split(',')
        _keywords_list = (list(map(str.strip, _keywords)))
        return dict.fromkeys(_keywords_list, 0)

    def _split_words_to_list(self):
        translator = str.maketrans('', '', string.punctuation)
        return self.text.translate(translator).lower().split()

    @staticmethod
    def _find_occurrence(keywords_dict, word):
        if word in keywords_dict.keys():
            keywords_dict.update([(word, keywords_dict.get(word, 0)+1)])
        return keywords_dict

    async def count_freq(self):
        keywords = self._split_keywords_to_dict()
        text_list = self._split_words_to_list()
        return reduce(self._find_occurrence, text_list, keywords)


class GetContent:
    @classmethod
    async def get_site_content(cls, url):
        async with aiohttp.ClientSession() as session:
            return await cls.fetch(session, url)

    @staticmethod
    async def fetch(session, url):
        async with async_timeout.timeout(10):
            async with session.get(url) as response:
                return await response.text()

class ParseHtml:

    @classmethod
    async def analyze(cls, content):
        soup = BeautifulSoup(content, "lxml")
        keywords = soup.find(attrs={"name":"keywords"}).get("content")
        return soup.get_text(), keywords

