from aiohttp import web
from logic.keywords import KeywordCounter, GetContent, ParseHtml


class KeywordsHandler:

    async def calculate_keywords(self, request):
        """  """
        post_data = await request.post()

        url = post_data.get('url', "")

        if url == "":
            return web.json_response({"Error":"-1"})

        # fetch data from passed url
        site_content = await GetContent.get_site_content(url)
                        
        # parse data form html content
        text, keyword = await ParseHtml.analyze(site_content)
                
        # calculate keywords frequency
        key = KeywordCounter(text, keyword)
        resp = await key.count_freq()

        return web.json_response(resp)
