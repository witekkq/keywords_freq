from aiohttp import web

import aiohttp_jinja2


class IndexHandler(web.View):

    @aiohttp_jinja2.template('index.html')
    async def index(self):
        return {'title': 'KeywordCounter'}
