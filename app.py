import asyncio
from aiohttp import web
from handlers.keywords import KeywordsHandler
from handlers.index import IndexHandler
import aiohttp_jinja2
import jinja2

HOST = '127.0.0.1'
PORT = 9001


def create_loop():
    return asyncio.get_event_loop()


async def init(withLoop):
    if withLoop:
        app = web.Application(loop=create_loop())
    else:
        app = web.Application()

    aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader('templates'))
    setup_routes(app)

    return app, HOST, PORT


def setup_routes(app):
    
    app.add_routes([web.get('/', IndexHandler.index)])

    key_handler = KeywordsHandler()
    
    app.add_routes([web.post('/results', key_handler.calculate_keywords)])


async def github_keywords_app():
    app, _host, _port = await init(False)
    return app


def main():
    loop = create_loop()
    app, host, port = loop.run_until_complete(init(True))
    web.run_app(app, host=host, port=port)


if __name__ == '__main__':
    main()
