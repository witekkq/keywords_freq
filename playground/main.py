import timeit
from keyword_counter import KeywordCounter

FILENAME = "9k_words_text.txt"
KEYWORDS = "abhorreant, Mea, commune"

def open_file(file_path):
    try:
        content_file = open(file_path)
    except IOError:
        print('Error: Cannot open file {path}'.format(path=file_path))
    else:
        with content_file:
            return content_file.read()


def speed_benchmark():
    keyword_counter = KeywordCounter(open_file(FILENAME), KEYWORDS)
    keyword_counter.count_freq()


print(timeit.timeit(stmt=speed_benchmark, number=10000))
