from functools import reduce
import string


class KeywordCounter:

    def __init__(self, text_input, keywords_list):
        self.text = text_input
        self.keywords_string = keywords_list

    def _split_keywords_to_dict(self):
        _keywords = self.keywords_string.lower().split(',')
        _keywords_list = (list(map(str.strip, _keywords)))
        return dict.fromkeys(_keywords_list, 0)

    def _split_words_to_list(self):
        translator = str.maketrans('', '', string.punctuation)
        return self.text.translate(translator).lower().split()

    @staticmethod
    def _find_occurrence(keywords_dict, word):
        if word in keywords_dict.keys():
            keywords_dict.update([(word, keywords_dict.get(word, 0)+1)])
        return keywords_dict

    def count_freq(self):
        keywords = self._split_keywords_to_dict()
        text_list = self._split_words_to_list()
        return reduce(self._find_occurrence, text_list, keywords)
